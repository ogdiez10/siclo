/**
 * Este componente contiene la sección donde se muestra el formulario de registro y checkout
 */
import { Component, OnInit } from '@angular/core';
import {
  FormControl,
  FormGroup,
  Validators,
  FormBuilder,
} from '@angular/forms';
import { MustMatch } from '../../_helpers/must-match.validator';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss'],
})
export class CheckoutComponent implements OnInit {
  registerForm: FormGroup;
  costoBisi: number = 42995; //Variable donde se obtiene el precio actual de la bisi
  costoEnvio: number = 500; //Variable donde se obtiene el costo de envío
  costoTotal: number;
  costoBisiCurrency: string; //Variables convertidas a string con coma de miles
  costoEnvioCurrency: string;
  costoTotalCurrency: string;

  constructor(private formBuilder: FormBuilder) {}

  onSubmit() {
    // TODO: Use EventEmitter with form value
    console.warn('¡Nueva venta!, llevar a motor de pago...');
  }

  ngOnInit(): void {
    this.costoTotal = this.costoBisi + this.costoEnvio;
    this.costoTotalCurrency = new Intl.NumberFormat('en-IN').format(
      this.costoTotal
    );
    this.costoBisiCurrency = new Intl.NumberFormat('en-IN').format(
      this.costoBisi
    );
    this.costoEnvioCurrency = new Intl.NumberFormat('en-IN').format(
      this.costoEnvio
    );

    this.registerForm = this.formBuilder.group(
      {
        nombre: new FormControl('', Validators.required),
        apellidos: new FormControl('', Validators.required),
        telefono: new FormControl('', Validators.required),
        correo: new FormControl('', [
          Validators.required,
          Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$'),
        ]),
        password: new FormControl('', Validators.required),
        passwordmatch: new FormControl('', Validators.required),
      },
      {
        validator: MustMatch('password', 'passwordmatch'),
      }
    );
  }
}
