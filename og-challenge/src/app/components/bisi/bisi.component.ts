/**
 * Este componente contiene la sección donde se muestra la biSí con sus planes de financiamiento.
 */
import { Component, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-bisi',
  templateUrl: './bisi.component.html',
  styleUrls: ['./bisi.component.scss'],
})
export class BisiComponent implements OnInit {
  closeResult: string;

  constructor(private modalService: NgbModal) {}

  open(content) {
    //Modal para mostrar los planes de financiamiento
    this.modalService
      .open(content, {
        ariaLabelledBy: 'modal-basic-title',
        windowClass: 'modal-siclo',
      })
      .result.then(
        (result) => {
          this.closeResult = `Closed with: ${result}`;
        },
        (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  ngOnInit(): void {}
}
