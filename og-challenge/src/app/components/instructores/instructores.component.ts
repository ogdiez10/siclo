/**
 * Este componente contiene la sección donde se muestran los instructores con el paginador
 */
import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-instructores',
  templateUrl: './instructores.component.html',
  styleUrls: ['./instructores.component.scss'],
})
export class InstructoresComponent implements OnInit {
  conversion: any;
  instructores: [];
  currentpage: any;
  totalpages: any;

  constructor(
    private http: HttpClient,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;

    this.activatedRoute.queryParams.subscribe((params) => {
      this.currentpage = this.activatedRoute.snapshot.params.page; //Obtener la página actual de la url
      //console.log(this.currentpage);
    });
  }

  ngOnInit(): void {
    this.http
      .get(
        'https://api.siclo.com/api/v2/plus/instructors/?format=json&page_size=10&page=' +
          this.currentpage
      )
      .subscribe((data) => {
        this.conversion = data;
        //console.log(this.conversion);
        this.instructores = this.conversion.data.results; //Guardamos el object instructores
        this.totalpages = this.conversion.data.total_pages; //Obtenemos el # de páginas totales de la API
      });
  }

  arrayPages(): number[] {
    return [...Array(this.totalpages).keys()].map((i) => i + 1); //Array con los números de página
  }
}
