import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BisiComponent } from './components/bisi/bisi.component';
import { InstructoresComponent } from './components/instructores/instructores.component';
import { CheckoutComponent } from './components/checkout/checkout.component';
import { HomeComponent } from './components/home/home.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'bisi', component: BisiComponent },
  { path: 'instructores/:page', component: InstructoresComponent },
  { path: 'checkout', component: CheckoutComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
